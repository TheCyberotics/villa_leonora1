-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2016 at 06:16 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `villa_leonora`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE IF NOT EXISTS `amenities` (
  `amen_id` int(100) NOT NULL,
  `amen_name` varchar(100) NOT NULL,
  `amen_desp` varchar(100) NOT NULL,
  `amen_image` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`amen_id`, `amen_name`, `amen_desp`, `amen_image`) VALUES
(1, 'Park', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit', 'pics/1.jpg'),
(2, 'Bedroom 1', 'Aenean commodo ligula eget dolor. Aenean massa.', 'pics/2.jpg'),
(3, 'Sofa', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ', 'pics/3.jpg'),
(4, 'Concierge', 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. ', 'pics/4.jpg'),
(5, 'Hallway', 'Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.', 'pics/5.jpg'),
(6, 'Great View', ' In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 'pics/6.jpg'),
(7, 'Sight Seeing ', ' Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.', 'pics/7.jpg'),
(8, 'Annex', 'Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.', 'pics/8.jpg'),
(9, 'Swimming Pool', ' Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.', 'pics/9.jpg'),
(10, 'Coffee Hub', 'Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.', 'pics/10.jpg'),
(11, 'Great View 3', 'Nam eget dui. Etiam rhoncus.', 'pics/11.jpg'),
(12, 'Bedroom 2', ' Maecenas tempus, tellus eget condimentum ', 'pics/12.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(100) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `firstname`, `lastname`, `email`, `comment`) VALUES
(1, 'Gina', 'Bulgado', 'gina@yahoo.com', '    gina comment            '),
(2, 'Gina', 'Bulgado', 'gina@yahoo.com', '    gina comment            '),
(3, 'Gina', 'Bulgado', 'gina@yahoo.com', '      fbgfbgbgfbfgbgb          '),
(4, 'Allan', 'Cayateno', 'allan@yahoo.com', '   from allan             '),
(5, 'Allan', 'Cayateno', 'allan@yahoo.com', '   from allan             '),
(6, 'Allan', 'Cayateno', 'allan@yahoo.com', '   from allan             '),
(7, 'Allan', 'Cayateno', 'allan@yahoo.com', '                '),
(8, 'Allan', 'Cayateno', 'allan@yahoo.com', '                '),
(9, 'vel', 'gauma', 'vel@yahoo.com', '                ATEH OKE AHH'),
(10, 'Erick Jason', 'Batuto', 'ejbatuto@hotmail.com', 'Extra bed for 2'),
(11, 'Joken', 'Villanueva', 'joken000189561@gmail.com', '                '),
(12, 'HATCH', 'VILLANUEVA', 'hatchvillanueva16@gmail.com', '                '),
(13, 'Joken', 'Villanueva', 'joken000189561@gmail.com', '                '),
(14, 'Erick Jason', 'Batuto', 'ejbatuto@hotmail.com', '                '),
(15, 'sdfsdfsd', 'dsfds', 'sddf', '                '),
(16, 'Kevi', 'Gargar', 'fg', '                '),
(17, 'Joken', 'Villanueva', 'joken000189561@gmail.com', '                '),
(18, 'Jesyl', 'Gozon', 'jhasyl@yahoo.com', '                '),
(19, 'Joken', 'Villanueva', 'joken000189561@gmail.com', '                '),
(20, 'steve', 'flores', 'kevinflores_23@yahoo.com', '                '),
(21, 'Janno', 'Palacios', 'jano@yahoo.com', '                '),
(22, 'Janno', 'Palaciosa', 'jeanniebenillos@gmail.com', '                '),
(23, 'jo', 'jk', 'jeanniebenillos@gmail.com', '                '),
(24, 'jo', 'jk', 'jeanniebenillos@gmail.com', '                '),
(25, 'jo', 'jk', 'jeanniebenillos@gmail.com', '                '),
(26, 'jo', 'jk', 'jeanniebenillos@gmail.com', '                '),
(27, 'sad', 'sad', 'j@yahoo.com', '                asdasdas'),
(28, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(29, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(30, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(31, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(32, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(33, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(34, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(35, 'xxxx', 'fsdf', 'super_admin@gmail.com', '                '),
(36, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(37, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(38, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(39, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(40, 'Jacob', 'Njega', 'njega@gmail.com', '                '),
(41, 'Ror', 'Tuano', 'tuanorory@gmail.com', '                '),
(42, 'Ror', 'Tuano', 'tuanorory@gmail.com', '                '),
(43, 'Ror', 'Tuano', 'tuanorory@gmail.com', '                '),
(44, 'Ror', 'Tuano', 'tuanorory@gmail.com', '                ');

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE IF NOT EXISTS `guest` (
  `guest_id` int(30) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`guest_id`, `firstname`, `lastname`, `country`, `city`, `address`, `zip`, `phone`, `email`, `password`) VALUES
(1, 'Marghie', 'Tuano', '', '', '', '', '+639065725474', 'angelespejo03@gmail.com', 'angel03');

-- --------------------------------------------------------

--
-- Table structure for table `ozekimessagein`
--

CREATE TABLE IF NOT EXISTS `ozekimessagein` (
  `id` int(11) NOT NULL,
  `sender` varchar(30) DEFAULT NULL,
  `receiver` varchar(30) DEFAULT '+254714812921',
  `msg` varchar(160) DEFAULT NULL,
  `senttime` varchar(100) DEFAULT NULL,
  `receivedtime` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `msgtype` varchar(160) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ozekimessagein`
--

INSERT INTO `ozekimessagein` (`id`, `sender`, `receiver`, `msg`, `senttime`, `receivedtime`, `operator`, `msgtype`, `reference`) VALUES
(1, '3D0B93C2F4F8FDD6', '+254714812921', 'ozeki message server trial - www.ozeki.hu', '2012-03-29 09:54:40', '2012-03-29 09:54:36', 'Safaricom', 'SMS:TEXT', '7394143'),
(2, '3D0B93C2F4F8FDD6', '+254714812921', 'Sorry,your account balance is not sufficient to complete this request.Please top up and try again.', '2012-03-29 09:54:58', '2012-03-29 09:54:54', 'Safaricom', 'SMS:TEXT', '7916704'),
(3, '3D0B93C2F4F8FDD6', '+254714812921', 'Sorry,your account balance is not sufficient to complete this request.Please top up and try again.', '2012-03-29 09:54:45', '2012-03-29 09:54:57', 'Safaricom', 'SMS:TEXT', '4474671'),
(4, '3D0B93C2F4F8FDD6', '+254714812921', 'ozeki message server trial - www.ozeki.hu', '2012-03-29 09:55:12', '2012-03-29 09:55:10', 'Safaricom', 'SMS:TEXT', '8415351');

-- --------------------------------------------------------

--
-- Table structure for table `ozekimessageout`
--

CREATE TABLE IF NOT EXISTS `ozekimessageout` (
  `id` int(11) NOT NULL,
  `sender` varchar(30) DEFAULT '+254714812921',
  `receiver` varchar(30) DEFAULT NULL,
  `msg` varchar(160) DEFAULT NULL,
  `senttime` varchar(100) DEFAULT NULL,
  `receivedtime` varchar(100) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'send',
  `msgtype` varchar(160) DEFAULT 'SMS:TEXT',
  `operator` varchar(100) DEFAULT 'Safaricom'
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ozekimessageout`
--

INSERT INTO `ozekimessageout` (`id`, `sender`, `receiver`, `msg`, `senttime`, `receivedtime`, `reference`, `status`, `msgtype`, `operator`) VALUES
(1, '+254714812921', '+254726667000', 'Thank You Jacob for making your reservation\n You are expected to pay sum of KES 300 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(2, '+254714812921', '+254726667000', 'Thank You Jacob for making your reservation\n You are expected to pay sum of KES 200 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(3, '+254714812921', '+254726667000', 'Thank You Jacob for making your reservation\n You are expected to pay sum of KES 300 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(4, '+254714812921', '+254720108418', 'Thank You juma for making your reservation\n You are expected to pay sum of KES 100 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(5, '+254714812921', '+254726667000', 'Thank You peterson  for making your reservation\n You are expected to pay sum of KES 400 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(6, '+254714812921', '+254712345678', 'Thank You Juma for making your reservation\n You are expected to pay sum of KES 1200 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(7, '+254714812921', '+254712345678', 'Thank You Juma for making your reservation\n You are expected to pay sum of KES 200 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(8, '+254714812921', '+254724789345', 'Thank You Njagi for making your reservation\n You are expected to pay sum of KES 300 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(9, '+254714812921', '+254712345678', 'Thank You Njagi for making your reservation\n You are expected to pay sum of KES 2200 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(10, '+254714812921', '+254724789345', 'Thank You Njagi for making your reservation\n You are expected to pay sum of KES 1500 for confirmation of reservation}', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(11, '+254714812921', '+254714812921', 'Thank You Simon  for making your reservation\n You are expected to pay sum of KES 600 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(12, '+254714812921', '+25471567892', 'Thank You Simon  for making your reservation\n You are expected to pay sum of KES 300 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(13, '+254714812921', '+254729475691', 'Thank You Peter for making your reservation\n You are expected to pay sum of KES 400 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(14, '+254714812921', '+254729475691', 'Thank You Peter for making your reservation\n You are expected to pay sum of KES 200 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(15, '+254714812921', '+254720108481', 'Thank You Felix for making your reservation\n You are expected to pay sum of KES 2100 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(16, '+254714812921', '+254727389289', 'Thank You Thuo for making your reservation\n You are expected to pay sum of KES 2200 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(17, '+254714812921', '+254729475691', 'Thank You Peter for making your reservation\n You are expected to pay sum of KES 2100 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(18, '+254714812921', '+254729475691', 'Thank You Peter for making your reservation\n You are expected to pay sum of KES 600 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(19, '+254714812921', '+254723455489', 'Thank You Israel for making your reservation\n You are expected to pay sum of KES 1800 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(20, '+254714812921', '+254729475691', 'Thank You Peter for making your reservation\n You are expected to pay sum of KES 1200 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(21, '+254714812921', '+254734567890', 'Thank You Jerop for making your reservation\n You are expected to pay sum of KES 2100 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(22, '+254714812921', '+254721241288', 'Thank You obote for making your reservation\n You are expected to pay sum of KES 1400 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(23, '+254714812921', '+254734567892', 'Thank You victor for making your reservation\n You are expected to pay sum of KES 1200 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(24, '+254714812921', '+254720108481', 'Thank You Felix for making your reservation\n You are expected to pay sum of KES 600 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(25, '+254714812921', '+254932898932', 'Thank You ken7832 for making your reservation\n You are expected to pay sum of KES 200 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(26, '+254714812921', '+254932898932', 'Thank You ken7832 for making your reservation\n You are expected to pay sum of KES 300 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(27, '+254714812921', '+254789123432', 'Thank You Wengi for making your reservation\n You are expected to pay sum of KES 1800 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(28, '+254714812921', '+254726667000', 'Thank You Jacob for making your reservation\n You are expected to pay sum of KES 100 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(29, '+254714812921', '+254714812921', 'Thank You simon for making your reservation\n You are expected to pay sum of KES 8400 for confirmation of reservation', NULL, NULL, NULL, 'sent', 'SMS:TEXT', 'Safaricom'),
(30, '+254714812921', '+254714812921', 'Thank You Simon for making your reservation\n You are expected to pay sum of KES 8000 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom'),
(31, '+254714812921', '+254714812234', 'Thank You Jacob for making your reservation\n You are expected to pay sum of KES 8400 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom'),
(32, '+254714812921', '+254720108418', 'Thank You Juma for making your reservation\n You are expected to pay sum of KES 24500 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom'),
(33, '+254714812921', '+254729475691', 'Thank You Peterson for making your reservation\n You are expected to pay sum of KES 27300 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom'),
(34, '+254714812921', '+254324324324', 'Thank You HATCH for making your reservation\n You are expected to pay sum of KES 0 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom'),
(35, '+254714812921', '+2543234234', 'Thank You HATCH for making your reservation\n You are expected to pay sum of KES 1500 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom'),
(36, '+254714812921', '+254323234234', 'Thank You HATCH for making your reservation\n You are expected to pay sum of KES 1500 for confirmation of reservation', NULL, NULL, NULL, 'send', 'SMS:TEXT', 'Safaricom');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `reservation_id` int(11) NOT NULL,
  `roomNo` int(50) NOT NULL,
  `guest_id` int(11) NOT NULL,
  `arrival` varchar(30) NOT NULL,
  `departure` varchar(30) NOT NULL,
  `adults` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `payable` int(11) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'pending',
  `booked` date NOT NULL,
  `confirmation` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`reservation_id`, `roomNo`, `guest_id`, `arrival`, `departure`, `adults`, `child`, `payable`, `status`, `booked`, `confirmation`) VALUES
(4, 14, 1, '2016-09-19', '2016-10-07', 1, 0, 32400, 'pending', '0000-00-00', '6yt5k0yb'),
(5, 15, 1, '2016-09-19', '2016-10-07', 1, 0, 27000, 'pending', '0000-00-00', '0ywfmuj4');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `roomNo` int(50) NOT NULL,
  `typeID` int(50) NOT NULL,
  `roomName` varchar(50) NOT NULL,
  `price` varchar(50) NOT NULL,
  `Adults` int(50) NOT NULL,
  `Children` int(10) NOT NULL,
  `roomImage` varchar(200) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`roomNo`, `typeID`, `roomName`, `price`, `Adults`, `Children`, `roomImage`) VALUES
(2, 83, 'Lion', '1500', 4, 1, 'rooms/del1.jpg'),
(3, 82, 'apartment', '1800', 4, 2, 'rooms/Stan1.JPG'),
(5, 83, 'Zion', '1750', 2, 1, 'rooms/102_4648.jpg'),
(8, 83, 'jason roomss', '1300', 1, 1, 'rooms/dsc03102.jpg'),
(14, 93, 'Family size#1', '1800', 4, 0, 'rooms/1.jpg'),
(15, 94, 'Family size#2', '1500', 4, 0, 'rooms/2.jpg'),
(16, 95, 'Family size#3', '1500', 4, 0, 'rooms/3.jpg'),
(17, 96, 'Family size#4', '1500', 4, 0, 'rooms/4.jpg'),
(18, 97, 'Cabin#1', '500', 2, 0, 'rooms/5.jpg'),
(19, 97, 'Cabin#2', '500', 2, 0, 'rooms/6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `roomtype`
--

CREATE TABLE IF NOT EXISTS `roomtype` (
  `typeID` int(50) NOT NULL,
  `typename` varchar(50) NOT NULL,
  `Desp` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roomtype`
--

INSERT INTO `roomtype` (`typeID`, `typename`, `Desp`) VALUES
(93, 'Family size aircondition', 'ddsdadas'),
(94, 'Family size regular #1', 'dasdasdasd'),
(95, 'Family size regular #2', 'daffafa'),
(96, 'Family size regular #3', 'dasaff'),
(97, 'Cabin', 'afafadfa');

-- --------------------------------------------------------

--
-- Table structure for table `tblsettings`
--

CREATE TABLE IF NOT EXISTS `tblsettings` (
  `ID` int(11) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `TYPE` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsettings`
--

INSERT INTO `tblsettings` (`ID`, `DESCRIPTION`, `TYPE`) VALUES
(1, '       On the year 2003, Elizabeth Gasataya and Family have started a business. It was Magbanua Beach Resort, located at # 3 Rojas Street, Kabankalan City Negros Occidental Philippines 6111. It was well renovated with 14 air conditioned rooms, Hot and Cold Shower, Cable Television and WIFI area. 									', 'About Us'),
(2, '      Provide our guests a unique experience, through which they connect with the best in our company,  												and to offer top quality service to our entire guest and provided comfort abundance.', 'Vision'),
(3, '   Magbanua Beach Resort  provides the best quality of services applying top quality guest house and conference facilities, in order to fulfill the best way in the relevant needs of every guest.', 'Mission');

-- --------------------------------------------------------

--
-- Table structure for table `useraccounts`
--

CREATE TABLE IF NOT EXISTS `useraccounts` (
  `ACCOUNT_ID` int(11) NOT NULL,
  `ACCOUNT_NAME` varchar(255) NOT NULL,
  `ACCOUNT_USERNAME` varchar(255) NOT NULL,
  `ACCOUNT_PASSWORD` text NOT NULL,
  `ACCOUNT_TYPE` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useraccounts`
--

INSERT INTO `useraccounts` (`ACCOUNT_ID`, `ACCOUNT_NAME`, `ACCOUNT_USERNAME`, `ACCOUNT_PASSWORD`, `ACCOUNT_TYPE`) VALUES
(5, 'Janno Palacios', 'janu@yahoo.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator'),
(6, 'jame yap', 'jame@yahoo.com', 'f144dcce05af4d40fa0aeba34b05f4472472a4de', 'Guest In-charge');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`amen_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`guest_id`);

--
-- Indexes for table `ozekimessagein`
--
ALTER TABLE `ozekimessagein`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ozekimessageout`
--
ALTER TABLE `ozekimessageout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`roomNo`);

--
-- Indexes for table `roomtype`
--
ALTER TABLE `roomtype`
  ADD PRIMARY KEY (`typeID`);

--
-- Indexes for table `tblsettings`
--
ALTER TABLE `tblsettings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `useraccounts`
--
ALTER TABLE `useraccounts`
  ADD PRIMARY KEY (`ACCOUNT_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `amen_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `guest_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ozekimessagein`
--
ALTER TABLE `ozekimessagein`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ozekimessageout`
--
ALTER TABLE `ozekimessageout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `roomNo` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `roomtype`
--
ALTER TABLE `roomtype`
  MODIFY `typeID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `tblsettings`
--
ALTER TABLE `tblsettings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `useraccounts`
--
ALTER TABLE `useraccounts`
  MODIFY `ACCOUNT_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
