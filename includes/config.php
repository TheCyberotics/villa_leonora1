<?php
/**
* Description:	The main class for Database.
* Author:	Angelica Espejo
* Date Created:	August 15, 2016
* Revised By:	Angelica Espejo		
*/

//Database Constants
defined('DB_SERVER') ? null : define("DB_SERVER","localhost");//define our database server
defined('DB_USER') ? null : define("DB_USER","root");		  //define our database user	
defined('DB_PASS') ? null : define("DB_PASS","");			  //define our database Password	
defined('DB_NAME') ? null : define("DB_NAME","villa_leonora"); //define our database Name

// defined('DB_SERVER') ? null : define("DB_SERVER","localhost");//define our database server
// defined('DB_USER') ? null : define("DB_USER","1028303");		  //define our database user	
// defined('DB_PASS') ? null : define("DB_PASS","angel03");			  //define our database Password	
// defined('DB_NAME') ? null : define("DB_NAME","1028303"); //define our database Name

$thisFile = str_replace('\\', '/', __FILE__);
$docRoot =$_SERVER['DOCUMENT_ROOT'];

$webRoot  = str_replace(array($docRoot, 'includes/config.php'), '', $thisFile);
$srvRoot  = str_replace('config/config.php','', $thisFile);

define('WEB_ROOT', $webRoot);
define('SRV_ROOT', $srvRoot);
?>