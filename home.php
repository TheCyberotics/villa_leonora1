

<!--End of Header-->
		<div  id="list_of_amenities_section"></div>
		<div class="col-xs-12 col-sm-12" >
			<!--<div class="jumbotron">-->
				<div class="">
					<div class="panel panel-default">
				
							<div class="panel-body">	
								<div class="col-xs-12 col-sm-12">
									<fieldset>
										<legend><h2 class="text-left">Gallery</h2></legend>

										<div class="container">
											
												
												<?php 

													$amen = new Amenities();
													$cur = $amen->listOfamenities();
													$counter = 1; 

													$leonora_phases = array("Phase 1", "Phase 2", "Phase 3", "Phase 4");
													$phases_counter = 0;
													echo '<div class="row">';
													echo '<ul class="row amenities">';
													echo "<h2>".$leonora_phases[$phases_counter]."</h2>";
													foreach($cur as $amenity){
														if($counter%3 == 0) {
															$image = WEB_ROOT . 'admin/mod_amenities/'.$amenity->amen_image;
															echo '<li  style="width:30%;height:200">';
																echo '<img class="img-responsive" style="width:100%; height:100%;border-radius:5px;" alt="'.$amenity->amen_desp.'"  src="'.$image.'">';
																echo ' <div class="text"><h4>&nbsp; '. $amenity->amen_name.'</h4></div>';
															echo '</li>';
															echo '</ul>';

															echo '</div>';
															$phases_counter++;
															if($phases_counter <= 3) {
															echo '<div class="row">';
																echo "<h2 >".$leonora_phases[$phases_counter]."</h2>";
																echo '<ul class="row amenities">';																
															}
															
														} else {

														$image = WEB_ROOT . 'admin/mod_amenities/'.$amenity->amen_image;
														echo '<li style="width:30%;">';
															echo '<img class="img-responsive" style="width:100%; height:100%;border-radius:5px;" alt="'.$amenity->amen_desp.'"  src="'.$image.'">';
															echo ' <div class="text"><h4>&nbsp;'. $amenity->amen_name.'</h4></div>';
														echo '</li>';

														}

														$counter++;

													}
												?>
										</div>
									</fieldset>	
								</div>
							</div>
						</div>	
				</div>

		</div>



<div  id="list_of_activities"></div>
<div class="col-xs-12 col-sm-12">
	<!--<div class="jumbotron">-->
		<div class="">
			<div class="panel panel-default">
		
					<div class="panel-body">	
						<div class="col-xs-12 col-sm-12">
							<fieldset>
								<legend><h2 class="text-left">Activity Timeline</h2></legend>

								<div class="container">
								    <ul class="timeline">

								        <li>
								          <div class="timeline-badge"><i class="glyphicon glyphicon-credit-card"></i></div>
								          <div class="timeline-panel">
								            <div class="timeline-heading">
								              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
<!-- 										              <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 hours ago via Twitter</small></p> -->
								            </div>
								            <div class="timeline-body">
								              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá ,
								               depois divoltis porris, paradis. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis
								                 mais bolis eu num gostis.</p>
								            </div>
								          </div>
								        </li>


								        <li class="timeline-inverted">
								          <div class="timeline-panel">
								            <div class="timeline-heading">
								              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
								            </div>
								            <div class="timeline-body">
								              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
								            </div>
								          </div>
								        </li>



								        <li>
								          <div class="timeline-panel">
								            <div class="timeline-heading">
								              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
								            </div>
								            <div class="timeline-body">
								              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis., nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. .</p>
								            </div>
								          </div>
								        </li>
								        <li class="timeline-inverted">
								          <div class="timeline-badge success"><i class="glyphicon glyphicon-thumbs-up"></i></div>
								          <div class="timeline-panel">
								            <div class="timeline-heading">
								              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
								            </div>
								            <div class="timeline-body">
								              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis.  quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim.</p>
								            </div>
								          </div>
								        </li>

								    </ul>
								</div>


							</fieldset>	
						</div>
					</div>
				</div>	
		
			
		</div>

</div>



	<div  id="about_us"></div>
	 <div class="col-xs-12 col-sm-12">
			<!--<div class="jumbotron">-->
				<div class="">
					<div class="panel panel-default">
				
							<div class="panel-body">	
								<div class="col-xs-12 col-sm-12">

								<fieldset>
								<fieldset>
										<legend><h2 class="text-left">About</h2></legend>
										<?php 
										$setting = New Setting();
										$result = $setting->single_setting(1);

										echo '<p>'.$result->DESCRIPTION.' </p>';

										?>
										 

									</fieldset>
									<hr/>
									<fieldset>

										<legend><h2 class="text-left">Company Mission</h2></legend>
										<?php 
										$setting = New Setting();
										$result = $setting->single_setting(2);

										echo '<p>'.$result->DESCRIPTION.' </p>';

										?>
										 
									</fieldset>	

									<fieldset>
										<legend><h2 class="text-left">Company Vision</h2></legend>
										<?php 
										$setting = New Setting();
										$result = $setting->single_setting(3);

										echo '<p>'.$result->DESCRIPTION.' </p>';

										?> 
									</fieldset>
									
									 </fieldset>

								</div>
							</div>
						</div>		
				
					
				</div>
		<!--	</div>-->
		</div>

<div  id="visit_us"></div>
		<div class="col-xs-12 col-sm-9">
			<!--<div class="jumbotron">-->
				<div class="">
					<div class="panel panel-default">
				
							<div class="panel-body">	
								<div class="col-xs-12 col-sm-12">
									<fieldset>
										<legend><h2 class="text-left" >Visit Us</h2></legend>
										 
										<div style="overflow:hidden;height:500px;width:100%;">
										<div id="gmap_canvas" style="height:500px;width:100%;"></div>
										<style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
										<a class="google-map-code" href="http://www.trivoo.net/gutscheine/schwab/" id="get-map-data">trivoo</a>
										</div>
										<script type="text/javascript">

										 function init_map(){
										 	 var map;
										 	 var marker;
										 	var myOptions = {
										 		zoom:17,
										 		center:new google.maps.LatLng(9.976595986836374,122.60393772617874),
										 		mapTypeId: google.maps.MapTypeId.HYBRID};
										 		map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
										 		marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(9.976595986836374, 122.60393772617874)});
										 		infowindow = new google.maps.InfoWindow({content:"<b>Magbanua's Beach Resort</b><br/>Negros South Road, Cauayan<br/>6111 Sipalay City" });
										 		google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});
										 		infowindow.open(map,marker);}

										 		google.maps.event.addDomListener(window, 'load', init_map);
										 		</script>
											<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAIKwoHjt9fI5fvXWkynhXBjORPK0uXHw&callback=init_map" async defer></script>
									 <br /><small>View 
									<a href="https://www.google.com.ph/maps/place/Villa+Leonor+Private+Pool/@14.2069287,121.06537,11z/data=!4m8!1m2!2m1!1sVilla+Leonora+Resort+pansol!3m4!1s0x0:0xaf0a58c2876a9fb!8m2!3d14.1805176!4d121.2020731" style="color:#0000FF;text-align:left">
									My Saved Places</a> in a larger map</small>	
									<address>Villa Leonora Resort<br/> 
									Pansol<br/> 
									Calamba City<br/> 
									Laguna</address> 

							</fieldset>		
								</div>
							</div>
						</div>	
				
					
				</div>
		<!--	</div>-->
		</div>
		<!--/span--> 
		<!--Sidebar-->
			<?php include'sidebar.php';?>

	<!--/row-->

