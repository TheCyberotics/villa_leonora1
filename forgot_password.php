<?php

require_once("includes/initialize.php");  
  $sent_response = "";

  if (isset($_REQUEST['forgotten_email']))  {

    $user_type = $_REQUEST['user_type'];
    $quer_key = "";

    if($user_type == "useraccounts") {
      $quer_key = "ACCOUNT_USERNAME";      
    } else {
      $quer_key = "email";      
    }

    $forgotten_email = $_REQUEST['forgotten_email'];

    $mydb->setQuery('SELECT *  FROM '. $user_type . ' WHERE '.$quer_key.'='. '\''.$forgotten_email.'\'');

    //$mydb->setQuery('SELECT *  FROM \''. $user_type . '\' WHERE'. $quer_key.'='. '\''.$forgotten_email.'\'');
    $cur = $mydb->loadResultList();

    if(count($cur) != 0) {
      foreach($cur as $guest){
      if($user_type == "useraccounts") {
        $guest_email= $guest->ACCOUNT_USERNAME;

        $subject = "Villa Leonora's Forgotten Password Request";
        $header="From: Villa Leonora";
        $message =  "Villa Leonora's Forgotten Password Request\n\nDear ". $guest->ACCOUNT_NAME .",\n\n Your password is ". $guest->ACCOUNT_PASSWORD ." \n\nThank you,\n Angelica Espejo";
        mail($guest_email,$subject,$message,$header);

        $sent_response = "You can now retrieve your forgotten password via the sent email. Thank you!";      
      } else {
        $guest_email= $guest->email;

        $subject = "Villa Leonora's Forgotten Password Request";
        $header="From: Villa Leonora";
        $message =  "Villa Leonora's Forgotten Password Request\n\nDear ". $guest->firstname .",\n\n Your password is ". $guest->password ." \n\nThank you,\n Angelica Espejo";
        mail($guest_email,$subject,$message,$header);

        $sent_response = "You can now retrieve your forgotten password via the sent email. Thank you!";     
      }
            

      } 
    } else {
        $sent_response = "Your search did not return any results. Please try again with another email. "; 
    }

  }
?>

<div  id="forgot_password"></div>
<div class="container">
	<div class="row">
	
        <div class="col-md-6 col-md-offset-3">

  <div >
    <h5 style="font-weight:bold;text-align:center; color:#FF5722; "><?php  echo $sent_response;  ?></h5>
    <h4 class="">

      Forgot your password?
    </h4>
    <form accept-charset="UTF-8" role="form" action="" id="forgot_password_request_form" method="post">
      <fieldset>
        <span class="help-block">
          Email address you use to log in to your account
          <br>
          We'll send you an email with your forgotten password. Thank you!
        </span>
        <select name="user_type" class="form-control" id="user_type">
        <option value=""> Choose User</option>
        <option value="useraccounts">Admin</option>
        <option value= "guest">Guest</option>
      </select>
      <br />
        <div id= "email_field_for_forgotten_password">
          <div class="form-group input-group">
            <span class="input-group-addon">
              @
            </span>
            <input class="form-control" placeholder="Email" name="forgotten_email" type="email" required="">
          </div>
          <button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">
            Send
          </button>
          <br />

        </fieldset>
      </div>
    </form>
  </div>
</div>
	</div>
</div>

<script>
$(document).ready(function() {
  $("#email_field_for_forgotten_password").hide();

  $("#user_type").on("change", function() {
      $("#email_field_for_forgotten_password").show();
  });

});

</script>