<?php
require_once("../../includes/initialize.php");
require "../../includes/twilio-master/Twilio/autoload.php";

use Twilio\Rest\Client;

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch ($action) {
	case 'modify' :
	dbMODIFY();
	break;
	
	case 'delete' :
	dbDELETE();
	break;
	
	case 'deleteOne' :
	dbDELETEONE();
	break;
	case 'confirm' :
	doConfirm();
	break;
	case 'cancel' :
	doCancel();
	break;
	case 'checkin' :
	doCheckin();
	break;
	case 'checkout' :
	doCheckout();
	break;
	}
function doCheckout(){

	$id = $_GET['id'];

	$res = new Reservation();
	$res->status = 'Checkedout';
	$res->update($id); 
					
	message("Reservation Upadated successfully!", "success");
	redirect('index.php');

}
function doCheckin(){
$id = $_GET['id'];

$res = new Reservation();
$res->status = 'Checkedin';
$res->update($id); 
 

//  // send e-mail to ...
// $email="angelicaespejo@rocketmail.com";
// $to=$email;

// // Your subject
// $subject="Your confirmation link here";

// // From
// $header="from: your name <your email>";

// // Your message
// $message="Your Comfirmation link \r\n";
// $message.="Click on this link to activate your account \r\n";
// $message.="http://www.yourweb.com/confirmation.php?passkey=$confirm_code";

// // send email
// $sentmail = mail($to,$subject,$message,$header);
  

// // if your email succesfully sent
// if($sentmail){
// echo "Your Confirmation link Has Been Sent To Your Email Address.";
// }
// else {
// echo "Cannot send Confirmation link to your e-mail address";
// }
message("Reservation Updated successfully!", "success");
redirect('index.php');

}


function doCancel(){
$id = $_GET['res_id'];

$res = new Reservation();
$res->status = 'Cancelled';
$res->update($id); 
				
message("Reservation Updated successfully!", "success");
redirect('index.php');

}
function doConfirm(){
$id 		= $_GET['res_id'];
$name 		= $_GET['name'];
$email 		= $_GET['email'];
$phone 		= $_GET['phone'];
$arrival 	= $_GET['arrival'];
$departure 	= $_GET['departure'];
$payable 	= $_GET['payable'];


$res = new Reservation();
$res->status = 'Confirmed';
$res->update($id); 
 

$subject = "Villa Leonora's Booking Confirmation";
$header="From: Villa Leonora";
$message =  "Villa Leonora's Resort Booking Confirmation\n\nDear ". $name .",\n\n Your Villa Leonora reservation has been confirmed. Your arrival date will be on ". $arrival ." and your departure date will be on " . $departure. " and the total cost is ". $payable. ". \n\nThank you,\n Angelica Espejo";

mail($email,$subject,$message,$header);
//SEND EMAIL 

try {
	$sid = "AC1d0a421b2b9c559a489cfc0b83a5f54a";
	$token = "cc385431b390c76407a29fb11d64de8b";
	$client = new Client($sid, $token);

	$client->messages->create(
	    "+639065725474", 
	    array(
	        'from' => '(201) 676-9883',
	        'body' => "Hi ". $name .", Your Villa Leonora reservation has been confirmed. Your arrival date will be on ". $arrival ." and your departure date will be on " . $departure. " and the total cost is ". $payable. ". Thank you, From Angelica Espejo"
	    )
	);
	message("The reservation of ". $name ." has been confirmed!", "success");
	redirect('index.php');	
} catch (Exception $e) {
	message("Sorry for the inconvience. The message hasn't been sent 'cause of the SMS problem. Thank you!", "error");
	redirect('index.php');
}



}	

?>